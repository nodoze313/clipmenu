clipText = clipText.replace(/\\/g,'');

if(!isNaN(clipText)) {
var date = new Date();
date.setTime(clipText * 1000);
var formattedTime = date.toLocaleString();
return formattedTime;
}
else
return "Invalid timestamp";
